import csv

magnet_val_list = list()
with open('VELA.csv', 'rt') as infile:
    reader = csv.DictReader(infile)
    for row in reader:
        detector=''
        GOOD=True
        det=slope=inter=maxa=a=I=d=mlen=qtype=''
        for key, value in row.items():
          if key=='Designation':
            det=value
          if key=='Slope':
            if value=='': GOOD=False
            if value=='#N/A': GOOD=False
            slope=value
          if key=='Intercept':
            inter=value
          if key=='max current [A]':
            if value == '' : value = '0'
            maxa=value
          if key=='a [units/A²]':
            a=value
          if key=='I0 [A]':
            I=value
          if key=='d [units]':
            d=value
          if key=='Magnetic length [mm]':
            mlen=value
          if (key=='Type') and ('Quadrupole' in value):
            qtype='1'
          if (key=='Type') and ('Quadrupole' not in value):
            qtype='0'
        if GOOD: print('{"'+det+'","'+slope+'","'+inter+'","'+maxa+'","'+a+'","'+I+'","'+d+'","'+mlen+'",'+qtype+',"299.792", "0.785398", "3.14159"}')
